const jsonServer = require('json-server')
const jsonServerAuth = require('json-server-auth')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
//const routerCustom = jsonServer.rewriter(router('routes.json')
const middlewares = jsonServer.defaults()
// middlewares.push(jsonServerAuth)

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)

// Set default middlewares (logger, static, cors and no-cache)
server.use('/api', middlewares)

server.db = router.db;

server.use((req, res, next) => {
  if (req.method === 'POST' && req.path === '/api/login') {
    req.body.email = req.body.username
    console.log('filter', req.path, req.body.email)
  }
  next()
})

server.use('/api', jsonServerAuth)


// Authorized
// server.use((req, res, next) => {
//   if (isAuthorized(req)) { // add your authorization logic here
//     next() // continue to JSON Server router
//   } else {
//     res.sendStatus(401)
//   }
//  })

// Add custom routes before JSON Server router
// server.get('/echo', (req, res) => {
//   res.jsonp(req.query)
// })

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
// server.use(jsonServer.bodyParser)
// server.use((req, res, next) => {
//   if (req.method === 'POST') {
//     req.body.createdAt = Date.now()
//   }
//   // Continue to JSON Server router
//   next()
// })

// In this example we simulate a server side error response
// router.render = (req, res) => {
//   res.status(500).jsonp({
//     error: "error message here"
//   })
// }

// Use default router
server.use('/api', jsonServer.rewriter({
  "/security/host-context/:hostname": "/contratantes/2",
  "/security/user-context/:user": "/contratantes",

}))
server.use('/api', router)
server.listen(3000, () => {
  console.log('JSON Server is running')
})
