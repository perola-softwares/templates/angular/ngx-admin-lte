import { Component, OnInit } from '@angular/core';


import * as locales from 'ngx-bootstrap/locale';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { SideBarService } from 'ngx-admin-lte';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'showcase-ngx-admin-lte';
  constructor(private sideBarService: SideBarService) {

    //this.defineLocales();

  }

  ngOnInit() {
    this.sideBarService.load([{
      name: 'Index',
      icon: 'fas fa-tachometer-alt',
      path: '/index',
      badge: 'new',
      children: [
        { name: 'Index1', path: '/index/1', icon: 'far fa-circle', badge: 'new' },
        { name: 'Index2', path: '/index/2', icon: 'far fa-circle', badge: '12' },
        { name: 'Index3', path: '/index/3', icon: 'far fa-circle' }
      ]
    }]);
  }

  // defineLocales() {
  //   console.log(locales);
  //   for (const locale in locales) {
  //       defineLocale(locales[locale].abbr, locales[locale]);
  //   }
  // }


}
