import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { AppComponent } from './app.component';
import { MainComponent, LoginComponent } from 'ngx-admin-lte';
import { IndexComponent } from './components/pages/index/index.component';
import { AuthGuard, MenuRoutes } from '@perolasoft/ngrx-template';

const routes: MenuRoutes = [
  {
    name: 'Index',
    description: 'Página inicial',
    path: '',
    redirectTo: '/index',
    pathMatch: 'full'
  },
  {
    name: 'Login',
    description: 'Login',
    path: 'login',
    component: LoginComponent,
  },
  {
    name: 'Home',
    description: 'Página inicial',
    path: 'index',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        name: 'Index1',
        description: 'Página inicial 1',
        path: '1',
        component: IndexComponent
      },
      {
        name: 'Index2',
        description: 'Página inicial 2',
        path: '2',
        component: IndexComponent
      },
      {
        name: 'Index3',
        description: 'Página inicial 3',
        path: '3',
        component: IndexComponent
      }

    ]
  },
  {
    name: 'Main',
    description: 'Página inicial',
    path: '**',
    component: MainComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
