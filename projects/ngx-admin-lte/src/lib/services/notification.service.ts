import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { Notification } from '../models/notification';
import { loadNotificationAction } from '@perolasoft/ngrx-template';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  // public readonly notification$: Subject<Notification[]> = new BehaviorSubject([]);
  // private current: Notification[];

  // constructor() {
  //   this.set([]);
  // }

  // public set(notifications: Notification[]) {
  //   this.current = notifications;
  //   this.notification$.next(this.current);
  // }

  // public append(notification: Notification) {
  //   this.current.push(notification);
  //   this.notification$.next(this.current);
  // }


  constructor(
    private store: Store<{ notifications: Notification[] }>) {
  }

  public load(notifications: Notification[]): void {
    this.store.dispatch(loadNotificationAction({ notifications }));
  }

}
