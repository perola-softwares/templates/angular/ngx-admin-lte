import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { MenuRoutes } from '@perolasoft/ngrx-template';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  private current: MenuRoutes;
  public readonly itens$: Subject<MenuRoutes> = new BehaviorSubject([]);

  constructor() {
    this.set([]);
  }

  public set(itens$: MenuRoutes) {
    this.current = itens$;
    this.itens$.next(this.current);
  }

  public append(itens$: MenuRoutes) {
    this.current.push(...itens$);
    this.itens$.next(this.current);
  }

}
