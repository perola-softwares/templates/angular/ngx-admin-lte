import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NgxAdminLteComponent } from './ngx-admin-lte.component';

describe('NgxAdminLteComponent', () => {
  let component: NgxAdminLteComponent;
  let fixture: ComponentFixture<NgxAdminLteComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxAdminLteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxAdminLteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
