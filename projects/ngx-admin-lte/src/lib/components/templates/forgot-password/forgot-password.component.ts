import { NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_ICON } from './../../../util/constantes';
import { Observable, Subscription, EMPTY } from 'rxjs';
import { ConfigService } from './../../../services/config.service';
import { setTheme } from 'ngx-bootstrap/utils';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  loginAction,
  AlertService,
  Context,
  SecurityService,
  ExceptionUtil,
  AlertType,
  Alert,
} from '@perolasoft/ngrx-template';
import { NgxAdminLteModuleConfig } from '../../../models/config';
import { tap, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'adminlte-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  public context: Context = null;
  public contextResolveSubscription: Subscription;
  public contextListSubscription: Subscription;
  public usernameValueChangeSubscription: Subscription;
  public classIconContext;

  public loginForm = this.fb.group({
    context: [''],
    username: ['', Validators.required],
  });

  public config: NgxAdminLteModuleConfig;
  public loginRouter: string;

  constructor(
    private fb: UntypedFormBuilder,
    private securityService: SecurityService,
    private configService: ConfigService,
    private alertService: AlertService
  ) {
    this.config = configService.getConfig();
    this.loginRouter = this.config.router.login;
    this.classIconContext = configService.getConfigProperty(
      NGRX_ADMINLTE_CONFIG_SECURITY_SERVICE_CONTEXT_ICON
    );
  }

  ngOnInit() {
    setTheme('bs4');

    const bodyElement = document.querySelector('body');
    const classList = bodyElement.classList;
    while (classList.length > 0) {
      classList.remove(classList.item(0));
    }
    classList.add('hold-transition', 'login-page');

    this.contextResolveSubscription = this.securityService
      .contextResolve()
      .subscribe({
        next: (contextSearch) => {
          this.context = contextSearch;
          this.loginForm.controls.context.setValue(contextSearch.id);
        },
        complete: () => {
          if (!this.context) {
            if (this.usernameValueChangeSubscription) {
              this.usernameValueChangeSubscription.unsubscribe();
            }
            this.usernameValueChangeSubscription = this.loginForm.controls.username.valueChanges
              .pipe(debounceTime(1000))
              .subscribe((value) => {
                if (this.loginForm.controls.username.valid) {
                  if (this.contextListSubscription) {
                    this.contextListSubscription.unsubscribe();
                  }
                  this.contextListSubscription = this.securityService
                    .contextList(value)
                    .subscribe({
                      next: (items) => {
                        if (items.length > 0) {
                          this.context = items[0];
                          this.loginForm.controls.context.setValue(items[0].id);
                        }
                      },
                    });
                }
              });
          }
        },
      });
  }

  ngOnDestroy(): void {
    if (this.contextResolveSubscription) {
      this.contextResolveSubscription.unsubscribe();
    }
    if (this.usernameValueChangeSubscription) {
      this.usernameValueChangeSubscription.unsubscribe();
    }
    if (this.contextListSubscription) {
      this.contextListSubscription.unsubscribe();
    }
  }

  public rememberPassword() {
    if (this.loginForm.controls.username.invalid) {
      this.alertService.add(
        new Alert(
          'security',
          AlertType.ERROR,
          'security.validation',
          'O campo "email" deve ser informado'
        )
      );
    } else {
      const param = this.loginForm.value;
      if (param.context === '') {
        delete param.context;
      }
      this.securityService.rememberPassword(param).subscribe(
        () => {
          this.alertService.add(
            new Alert(
              'security',
              AlertType.SUCCESS,
              'security.rememberPassword.success',
              'Siga as instruções recebida por email para reiniciar sua senha.'
            )
          );
        },
        (error) =>
          this.alertService.add(
            ExceptionUtil.createAlert(
              ExceptionUtil.convertGenericError(error),
              'security',
              AlertType.ERROR
            )
          )
      );
    }
  }
}
