import { NGRX_TEMPLATE_CONFIG_SECURITY_TOKEN_SESSION_STORAGE_KEY, loginUpdateTokenAction, MenuRoute, Context } from '@perolasoft/ngrx-template';
import { Store } from '@ngrx/store';
import { ConfigService } from './../../../services/config.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import {
  ActivatedRoute,
  Router,
  NavigationEnd
} from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { SecurityUtil } from '@perolasoft/ngrx-template';

declare var $: any;

@Component({
  selector: 'adminlte-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  public pageTitle: string;
  private subscriptionRouterEvents: Subscription;

  constructor(private route: ActivatedRoute, private router: Router, private configService: ConfigService, private store: Store) {}

  ngOnInit() {
    setTheme('bs4');

    const bodyElement = document.querySelector('body');
    const classList = bodyElement.classList;
    while (classList.length > 0) {
      classList.remove(classList.item(0));
    }
    classList.add('hold-transition', 'sidebar-mini');

    this.subscriptionRouterEvents = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(url => {
        this.configurePageTitle();
      });

    this.configurePageTitle();


    const rawToken = sessionStorage.getItem(this.configService.getConfigProperty(NGRX_TEMPLATE_CONFIG_SECURITY_TOKEN_SESSION_STORAGE_KEY));

    if (rawToken) {
      const principal = SecurityUtil.createPrincipalFromToken(rawToken);

      this.store.dispatch(
        loginUpdateTokenAction(
          { user: principal,
          jwtToken: principal.jwtToken, context: principal.context }));
    }

    // Inicialização do Layout admin-lte
    $('body').Layout();

  }

  private configurePageTitle() {
    this.pageTitle = this.route.children && this.route.children.length > 0
      ? (this.route.children[this.route.children.length - 1]
          .routeConfig as MenuRoute).description
      : (this.route.routeConfig as MenuRoute).description;
  }

  ngOnDestroy(): void {
    this.subscriptionRouterEvents.unsubscribe();
  }
}
