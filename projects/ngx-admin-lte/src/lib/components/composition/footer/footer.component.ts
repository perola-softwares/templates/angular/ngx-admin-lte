import { NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_FOOTER_ITEM_LEFT, NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_FOOTER_ITEM_RIGHT } from './../../../models/utils/constants';
import { Component, OnInit, Type } from '@angular/core';
import { ConfigService } from '@perolasoft/ngrx-template';

@Component({
  selector: 'adminlte-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public footerItemLeft: Type<any>[];
  public footerItemRight: Type<any>[];

  constructor(private configService: ConfigService) {
    this.footerItemLeft = this.configService.getConfigProperty(NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_FOOTER_ITEM_LEFT);
    this.footerItemRight = this.configService.getConfigProperty(NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_FOOTER_ITEM_RIGHT);
  }


  ngOnInit() {
  }

}
