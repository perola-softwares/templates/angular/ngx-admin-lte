import { Component, OnInit, ViewContainerRef, ViewChild, Type, Input, ComponentFactory, ComponentFactoryResolver } from '@angular/core';

@Component({
  selector: 'adminlte-header-custom-item',
  templateUrl: './header-custom-item.component.html',
  styleUrls: ['./header-custom-item.component.scss']
})
export class HeaderCustomItemComponent implements OnInit {

  @ViewChild('headerCustomItem', {read: ViewContainerRef, static: true })
  private headerCustom: ViewContainerRef;

  @Input()
  private componentType: Type<any>;

  constructor(private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
      const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(this.componentType);
      this.headerCustom.createComponent(factory);
  }

}
