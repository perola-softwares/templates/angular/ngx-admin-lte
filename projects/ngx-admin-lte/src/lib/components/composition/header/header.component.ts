import { NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_LEFT,
  NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_CENTER,
  NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_RIGHT } from './../../../models/utils/constants';
import { ConfigService } from './../../../services/config.service';
import { Component, OnInit, Type } from '@angular/core';

@Component({
  selector: 'adminlte-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public headerItemLeft: Type<any>[];
  public headerItemCenter: Type<any>[];
  public headerItemRight: Type<any>[];

  constructor(private configService: ConfigService) {
    this.headerItemLeft = this.configService.getConfigProperty(NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_LEFT);
    this.headerItemCenter = this.configService.getConfigProperty(NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_CENTER);
    this.headerItemRight = this.configService.getConfigProperty(NGX_ADMINLTE_CONFIG_TEMPLATE_MAIN_HEADER_ITEM_RIGHT);
  }

  ngOnInit() {
  }

}
