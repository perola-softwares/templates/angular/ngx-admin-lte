import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageComponent } from '../../components/message/message.component';
import { MessageService } from '../../services/message.service';

@NgModule({
  declarations: [
    MessageComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MessageComponent
  ],
  providers: [
    MessageService
  ]
})
export class MessageModule { }
