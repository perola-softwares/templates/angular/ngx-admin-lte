import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupModuleNotification } from '../../models/notification';
import { INJECT_TOKEN_NOTIFICATION } from '../../models/utils/inject.tokens';
import { NotificationComponent } from '../../components/notification/notification.component';
import { NotificationService } from '../../services/notification.service';

@NgModule({
  declarations: [
    NotificationComponent
  ],
  imports: [CommonModule],
  exports: [
    NotificationComponent
  ],
  providers: [
    NotificationService
  ]
})
export class NotificationModule {
  static forRoot(setup: SetupModuleNotification): ModuleWithProviders<NotificationModule> {
    return {
      ngModule: NotificationModule,
      providers: [
        { provide: INJECT_TOKEN_NOTIFICATION, useValue: setup }
      ]
    };
  }
}
