import { TestBed } from '@angular/core/testing';

import { NgxAdminLteService } from './ngx-admin-lte.service';

describe('NgxAdminLteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgxAdminLteService = TestBed.get(NgxAdminLteService);
    expect(service).toBeTruthy();
  });
});
