import { InjectionToken } from '@angular/core';
import {
  INJECT_TOKEN_NOTIFICATION_IDENTIFIER,
} from './constants';
import { SetupModuleNotification } from '../notification';

export const INJECT_TOKEN_NOTIFICATION = new InjectionToken<SetupModuleNotification>(
  INJECT_TOKEN_NOTIFICATION_IDENTIFIER,
  {
    providedIn: 'root',
    factory: (): SetupModuleNotification => ({
      groupByType: true,
      groups: [],
      maxPreview: 5,
      routeLinkAllNotifications: '/notifications'
    })
  }
);
