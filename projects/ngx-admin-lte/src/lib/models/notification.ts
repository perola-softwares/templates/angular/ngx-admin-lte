export interface Notification {
  type: string;
  description: string;
  date: Date;
  routeLink: string;
}

export interface SetupModuleNotification {
  groupByType: boolean;
  groups: { type: string, config: { icon: string, routeLink: string } }[];
  maxPreview: number;
  routeLinkAllNotifications: string;
}
