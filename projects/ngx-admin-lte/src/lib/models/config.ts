import { NotificationComponent } from './../components/notification/notification.component';
import { Type } from '@angular/core';
import { NgrxTemplateModuleConfig, NgrxTemplateModuleDetailConfig, NgrxTemplateModuleSecurityConfig, NgrxTemplateModuleSecurityServiceConfig, BsButtonLogoutComponent } from '@perolasoft/ngrx-template';
import { MessageComponent } from '../components/message/message.component';

export interface NgxAdminLteModuleConfig extends NgrxTemplateModuleConfig {
  sideBar?: NgxAdminLteModuleSideBarConfig;
  icons?: NgxAdminLteModuleIconsConfig;
  template?: NgxAdminLteModuleTemplateConfig;

}
export interface NgxAdminLteModuleTemplateConfig {
  main?: NgxAdminLteModuleMainTemplateConfig;
}

export interface NgxAdminLteModuleMainTemplateConfig {
  header?: NgxAdminLteModuleHeaderConfig;
  footer?: NgxAdminLteModuleFooterConfig;
}

export interface NgxAdminLteModuleFooterConfig {
  itemLeft?: Type<any>[];
  itemRight?: Type<any>[];
}

export interface NgxAdminLteModuleHeaderConfig {
  itemLeft?: Type<any>[];
  itemCenter?: Type<any>[];
  itemRight?: Type<any>[];
}

export interface NgxAdminLteModuleIconsConfig {
  context?: string;
}
export interface NgxAdminLteModuleSideBarConfig {
  labels?: Label[];
}

export interface Label {
  icon: string;
  text: string;
}


export const defaultConfig: NgxAdminLteModuleConfig = {
  detail: {
    name: 'AdminTLE',
    description: 'Showcase AdminLTE',
    version: '0.0.1',
    company: 'PerolaSoft',
    companyShortName: 'PerolaSoft',
    images: {
      iconSystemPath: 'assets/img/AdminLTELogo.png'
    }
  },
  template: {
    main: {
      header: {
        itemLeft: [],
        itemCenter: [],
        itemRight: [MessageComponent, NotificationComponent, BsButtonLogoutComponent]
      }
    }
  },
  icons: {
    context: 'fa fa-award'
  }
};

